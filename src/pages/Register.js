import {Button, Form} from 'react-bootstrap';
import {Fragment} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';

// import the hooks that are needed in our page
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function Register(){
	// Create 3 new states where we will store the value from input of the email, password and confirmPassword

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	const navigate = useNavigate();

	/*const [user, setUser] = useState(localStorage.getItem("email"));*/
	const {user, setUser} = useContext(UserContext);

	// create another state for the button
	const [isActive, setIsActive] = useState(false);

	useEffect(()=>{
		if(email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password, confirmPassword]);

	function register(event){
		event.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password,
				confirmPassword: confirmPassword
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);
			if(data === false){
				Swal.fire({
					title: "Registration failed!",
					icon: "error",
					text: "Please try again!"
				})
			}else{
				Swal.fire({
					title: "Registration successful!",
					icon: 'success',
					text: "You may now login!"
				})
				navigate('/login');
			}
		});

		/*localStorage.setItem("email", email);
		setUser(localStorage.getItem("email"));
		event.preventDefault();
		alert("You are now registered!");
		setEmail('');
		setPassword('');
		setConfirmPassword('');
		navigate("/");*/
	}

	return(
		user ?
		<Navigate to ="*"/>
		:
		<Fragment>
			<h1 className = "text-center mt-5">Register</h1>
			<Form className = "mt-5" onSubmit = {event => register(event)}>
				<Form.Group className="mb-3" controlId="formFirstName">
				    <Form.Label>First Name</Form.Label>
				    <Form.Control
				    	type="string"
				    	placeholder="First Name"
				    	value = {firstName}
				    	onChange = {event => setFirstName(event.target.value)}
				    	required
				    />
				</Form.Group>

				<Form.Group className="mb-3" controlId="formLastName">
				    <Form.Label>Last Name</Form.Label>
				    <Form.Control
				    	type="string"
				    	placeholder="Last Name"
				    	value = {lastName}
				    	onChange = {event => setLastName(event.target.value)}
				    	required
				    />
				</Form.Group>

			    <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control
			        	type="email"
			        	placeholder="Enter email"
			        	value = {email}
			        	onChange = {event => setEmail(event.target.value)}
			        	required
			        />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="formMobileNo">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control
			        	type="string"
			        	placeholder="Mobile no."
			        	value = {mobileNo}
			        	onChange = {event => setMobileNo(event.target.value)}
			        	required
			        />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Password</Form.Label>
			        <Form.Control
			        	type="password"
			        	placeholder="Password"
			        	value = {password}
			        	onChange = {event => setPassword(event.target.value)}
			        	required
			        />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="formConfirmPassword">
			        <Form.Label>Confirm Password</Form.Label>
			        <Form.Control
			        	type="password"
			        	placeholder="Confirm your password"
			        	value = {confirmPassword}
			        	onChange = {event => setConfirmPassword(event.target.value)}
			        	required
			        />
			    </Form.Group>

			    {/*In this code block we do conditional rendering depending on the state of our isActive*/}
			    {
			    	isActive?
			    	<Button variant="primary" type="submit">
			    	    Submit
			    	</Button>
			    	:
			    	<Button variant="danger" type="submit" disabled>
			    	    Submit
			    	</Button>
			    }

			</Form>
		</Fragment>
	)
}