// to create react app,
	// npx create-react-app project-name

// to run our react application
	// npm start

	// files to be removed:
	// from folder src:
		// App.test.js
		// index.css
		// reportWebVitals.js
		// logo.svg

	// we need to delete all the importations of the said files

// the "import" statement allows us to use the code/wxport modules from other files similar to how we use the "require" function in NodeJS

// React JS applies the concepts of rendering and mounting in order to display and create components
// Rendering refers to the process of calling/invoking a component returning set of instructions creating DOM

// Mounting is when REACT JS "renders or displays" the component the initial DOM based on the instructions

// using the event.target.value will capture the value inputted by the user on our input area;

	// in the dependencies in useEffect
		// 1. Single dependency [dependency]
			// on the initial load of the component the side effect/function will trigger and whenever changes occur on our dependency
		// 2. empty dependecy
			// the side effect will only run during the initial load
		// 3. multiple dependency [dependency1, dependency2, dependency3]
			// the side effect will run during the initial load and whenever the state of the dependencies change

// [Section] 'react-router-dom'
	// for us to be able to use the module/library across all of our pages we have to contain them with BrowserRouter/Router
	// Routes - we contain all of the routes in our react-app
	// Route - specific route wherein we will declare the url and also the component or page to be mounted

	// we use link if we want to go from one page to another page
	// we use NavLink if we want to change our page using our NavBar

	// The "localStorage.setItem allows us to manipulate the browser's localStorage property to store information indefinitely"

// [Section] Environment variable
	// environment variables are important in hiding the sensitive pieces of information like the backend API which can be exploited if added directly into our code